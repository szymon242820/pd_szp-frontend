import { KeycloakService } from "keycloak-angular";

export function initializeKeycloak(
  keycloak: KeycloakService
  ) {
    return () =>
      keycloak.init({
        config: {
          url: 'http://localhost:9000',
          realm: 'itex',
          clientId: 'angular-itex',
        },
        initOptions: {
          onLoad: 'check-sso',
          checkLoginIframe: false
        },
        loadUserProfileAtStartUp: true
      });
}