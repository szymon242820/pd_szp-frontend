import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ForbiddenComponent } from './component/forbidden/forbidden.component';
import { JobOfferComponent } from './component/job-offer/job-offer.component';
import { MainpageComponent } from './component/mainpage/mainpage.component';
import { TimesheetComponent } from './component/timesheet/timesheet.component';
import { WorkerDemandCcmComponent } from './component/worker-demand-ccm/worker-demand-ccm.component';
import { WorkerDemandHrComponent } from './component/worker-demand-hr/worker-demand-hr.component';
import { WorkerDemandTeamLeaderComponent } from './component/worker-demand-team-leader/worker-demand-team-leader.component';
import { WorkersActivitiesComponent } from './component/workers-activities/workers-activities.component';
import { WorkersDetailsComponent } from './component/workers-details/workers-details.component';
import { WorkersComponent } from './component/workers/workers.component';
import { AuthGuard } from './keycloak/guard/auth.guard';

const routes: Routes = [
  { path: '', component: MainpageComponent },
  { path: 'forbidden', component: ForbiddenComponent, canActivate: [AuthGuard] },
  { path: 'timesheet', component: TimesheetComponent, canActivate: [AuthGuard], data: {roles: ['DEVELOPER']} },
  { path: 'workers', component: WorkersComponent, canActivate: [AuthGuard], data: {roles: ['CCM']}},
  { path: 'job-offer', component: JobOfferComponent, canActivate: [AuthGuard], data: {roles: ['HR']}},
  { path: 'workers-details', component: WorkersDetailsComponent, canActivate: [AuthGuard], data: {roles: ['HR']}},
  { path: 'workers-activities', component: WorkersActivitiesComponent, canActivate: [AuthGuard], data: {roles: ['TEAM_LEADER']}},
  { path: 'worker-demand-tl', component: WorkerDemandTeamLeaderComponent, canActivate: [AuthGuard], data: {roles: ['TEAM_LEADER']}},
  { path: 'worker-demand-hr', component: WorkerDemandHrComponent, canActivate: [AuthGuard], data: {roles: ['HR']}},
  { path: 'worker-demand-ccm', component: WorkerDemandCcmComponent, canActivate: [AuthGuard], data: {roles: ['CCM']}},
  { path: '**', redirectTo: '' }
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
