import { APP_INITIALIZER, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatIconModule } from '@angular/material/icon';
import { MaterialExampleModule } from '../material.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ToolbarComponent } from './component/toolbar/toolbar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { initializeKeycloak } from './keycloak/init/keycloak-init.factory';
import { KeycloakService, KeycloakAngularModule } from "keycloak-angular";
import { TimesheetComponent } from './component/timesheet/timesheet.component';
import { WorkersComponent } from './component/workers/workers.component';
import { HttpClientModule } from '@angular/common/http';
import { MainpageComponent } from './component/mainpage/mainpage.component';
import { ForbiddenComponent } from './component/forbidden/forbidden.component';
import { ModalModule } from 'ngx-bootstrap/modal';
import { NgToastModule } from 'ng-angular-popup';
import { AddWorkdayModalComponent } from './component/timesheet/add-workday-modal/add-workday-modal.component';
import { EditWorkdayModalComponent } from './component/timesheet/edit-workday-modal/edit-workday-modal.component';
import { DeleteWorkdayModalComponent } from './component/timesheet/delete-workday-modal/delete-workday-modal.component';
import { WorkersActivitiesComponent } from './component/workers-activities/workers-activities.component';
import { ChangeDevTeamModalComponent } from './component/workers/change-dev-team-modal/change-dev-team-modal.component';
import { WorkersDetailsComponent } from './component/workers-details/workers-details.component';
import { WorkersDetailsModalComponent } from './component/workers-details/workers-details-modal/workers-details-modal.component';
import { WorkerDemandTeamLeaderComponent } from './component/worker-demand-team-leader/worker-demand-team-leader.component';
import { EditDemandModalComponent } from './component/worker-demand-team-leader/edit-demand-modal/edit-demand-modal.component';
import { CancelDemandModalComponent } from './component/worker-demand-team-leader/cancel-demand-modal/cancel-demand-modal.component';
import { WorkerDemandCcmComponent } from './component/worker-demand-ccm/worker-demand-ccm.component';
import { DemandChangeStatusCcmModalComponent } from './component/worker-demand-ccm/demand-change-status-ccm-modal/demand-change-status-ccm-modal.component';
import { WorkerDemandHrComponent } from './component/worker-demand-hr/worker-demand-hr.component';
import { DemandChangeStatusHrModalComponent } from './component/worker-demand-hr/demand-change-status-hr-modal/demand-change-status-hr-modal.component';
import { JobOfferComponent } from './component/job-offer/job-offer.component';
import { OfferEditModalComponent } from './component/job-offer/offer-edit-modal/offer-edit-modal.component';
import { OfferDeleteModalComponent } from './component/job-offer/offer-delete-modal/offer-delete-modal.component';

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    TimesheetComponent,
    WorkersComponent,
    MainpageComponent,
    ForbiddenComponent,
    AddWorkdayModalComponent,
    EditWorkdayModalComponent,
    DeleteWorkdayModalComponent,
    WorkersActivitiesComponent,
    ChangeDevTeamModalComponent,
    WorkersDetailsComponent,
    WorkersDetailsModalComponent,
    WorkerDemandTeamLeaderComponent,
    EditDemandModalComponent,
    CancelDemandModalComponent,
    WorkerDemandCcmComponent,
    DemandChangeStatusCcmModalComponent,
    WorkerDemandHrComponent,
    DemandChangeStatusHrModalComponent,
    JobOfferComponent,
    OfferEditModalComponent,
    OfferDeleteModalComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MatToolbarModule,
    BrowserAnimationsModule,
    MatIconModule,
    MaterialExampleModule,
    KeycloakAngularModule,
    HttpClientModule,
    NgToastModule,
    ModalModule.forRoot()
  ],
  exports: [
    MatIconModule
  ],
  providers: [
    {
      provide: APP_INITIALIZER,
      useFactory: initializeKeycloak,
      multi: true,
      deps: [KeycloakService],
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
