import { Teamleader } from "./teamleader";

export interface Devteam {
    id: number,
    name: string,
    teamLeader: Teamleader,
}
