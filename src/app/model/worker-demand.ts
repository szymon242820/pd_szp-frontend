import { Skill } from "./skill";
import { Specialization } from "./specialization";

export interface WorkerDemand {
    id: number,
    exp: string,
    spec: Specialization,
    skills: Skill[],
    status: string
}
