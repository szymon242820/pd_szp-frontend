import { Devteam } from "./devteam";
import { Skill } from "./skill";
import { Specialization } from "./specialization";

export interface Developer {
    id: number,
    name: string,
    surname: string,
    email: string,
    exp: string,
    spec: Specialization,
    skills: Skill[],
    devTeam: Devteam
}
