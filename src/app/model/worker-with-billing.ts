import { Devteam } from "./devteam";
import { Skill } from "./skill";
import { Specialization } from "./specialization";

export interface WorkerWithBilling {
    id: number,
    name: string,
    surname: string,
    email: string,
    exp: string,
    spec: Specialization,
    skills: Skill[],
    devTeam: Devteam,
    createdAt: string,
    bankAccNumber: string,
    salaryPerHour: number
}
