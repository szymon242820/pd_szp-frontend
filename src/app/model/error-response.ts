export interface errorResponse {
    message: string,
    timestamp: string,
}
