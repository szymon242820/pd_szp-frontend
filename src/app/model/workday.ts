import { WorkdayDuration } from "./workday-duration";

export interface Workday {
    id: number,
    description: string,
    date: string,
    duration: WorkdayDuration,
    status: string
}
