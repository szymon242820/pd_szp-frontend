import { Skill } from "./skill";
import { Specialization } from "./specialization";

export interface Joboffer {
    id: number,
    description: string,
    exp: string,
    spec: Specialization,
    skills: Skill[]
}
