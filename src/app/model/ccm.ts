export interface Ccm {
    id: number,
    name: string,
    surname: string,
    email: string
}
