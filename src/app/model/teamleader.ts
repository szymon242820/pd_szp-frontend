import { Ccm } from "./ccm";

export interface Teamleader {
    id: number,
    name: string,
    surname: string,
    email: string,
    ccm: Ccm
}
