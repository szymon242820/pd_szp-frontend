import { Skill } from "./skill";
import { Specialization } from "./specialization";
import { Workday } from "./workday";

export interface WorkerWithSpecifics {
    id: number,
    name: string,
    surname: string,
    email: string,
    exp: string,
    spec: Specialization,
    skills: Skill[],
    workdays: Workday[]
}
