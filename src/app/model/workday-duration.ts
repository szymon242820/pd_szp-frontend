export interface WorkdayDuration {
    hours: number,
    minutes: number
}
