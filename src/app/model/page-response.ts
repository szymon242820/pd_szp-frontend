export interface PageResponse {
    result: any[]
    pageNumber: number,
    pageSize: number,
    totalPages: number,
    totalElements: number
}
