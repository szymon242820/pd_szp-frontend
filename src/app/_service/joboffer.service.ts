import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Joboffer } from '../model/joboffer';
import { PageResponse } from '../model/page-response';

@Injectable({
  providedIn: 'root'
})
export class JobofferService {

  constructor(private http: HttpClient) { }

  public getJoboffers(pageNumber: number, pageSize: number): Observable<PageResponse> {
    return this.http.get<PageResponse>(`${environment.backendUrl}/${environment.apiVersion}/offer?pageNumber=${pageNumber}&pageSize=${pageSize}`);
  }
  public getJobofferById(jobOfferId: number): Observable<Joboffer> {
    return this.http.get<Joboffer>(`${environment.backendUrl}/${environment.apiVersion}/offer/${jobOfferId}`);
  }
  public putJoboffer(joboffer: Joboffer): Observable<Joboffer> {
    return this.http.put<Joboffer>(`${environment.backendUrl}/${environment.apiVersion}/offer`, joboffer);
  }
  public postJoboffer(joboffer: Joboffer): Observable<Joboffer> {
    return this.http.post<Joboffer>(`${environment.backendUrl}/${environment.apiVersion}/offer`, joboffer);
  }
  public deletJoboffer(id: number): Observable<Joboffer> {
    return this.http.delete<Joboffer>(`${environment.backendUrl}/${environment.apiVersion}/offer?jobOfferId=${id}`);
  }
}
