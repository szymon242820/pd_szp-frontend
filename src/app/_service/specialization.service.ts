import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Specialization } from '../model/specialization';

@Injectable({
  providedIn: 'root'
})
export class SpecializationService {

  constructor(private http: HttpClient) { }
  public getAllSpecializations(): Observable<Specialization[]>{
    return this.http.get<Specialization[]>(`${environment.backendUrl}/${environment.apiVersion}/specialization`);
  }
}
