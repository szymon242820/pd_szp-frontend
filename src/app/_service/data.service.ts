import { Injectable } from '@angular/core';
import { Skill } from '../model/skill';
import { Specialization } from '../model/specialization';
import { Workday } from '../model/workday';

@Injectable({
  providedIn: 'root'
})
export class DataService {
  private date!: Date;
  private workday! : Workday
  private workdayId!: number
  private workerId!: number
  private demandId!: number
  private jobOfferId!: number
  private skills!: Skill[]
  private specializations!: Specialization[]

  constructor() { }

  setDate(date: Date): void {
    this.date = date;
  }
  getDate(): Date {
    return this.date!;
  }
  
  setWorkday(workday: Workday): void {
    this.workday = workday
  }
  getWorkday(): Workday {
    return this.workday!;
  }

  setWorkdayId(id: number): void {
    this.workdayId = id;
  }
  getWorkdayId(): number {
    return this.workdayId!;
  }

  setWorkerId(workerId: number): void {
    this.workerId = workerId
  }
  getWorkerId(): number {
    return this.workerId
  }

  setDemandId(demandId: number): void {
    this.demandId = demandId
  }
  getDemandId(): number {
    return this.demandId
  }

  setSkills(skills: Skill[]): void {
    this.skills = skills
  }
  getSkills(): Skill[] {
    return this.skills
  }

  setSpecializations(specializations: Specialization[]): void {
    this.specializations = specializations
  }
  getSpecializations(): Specialization[] {
    return this.specializations
  }

  setJobOfferId(jobOfferId: number): void {
    this.jobOfferId = jobOfferId
  }
  getJobOfferId(): number {
    return this.jobOfferId
  }
}
