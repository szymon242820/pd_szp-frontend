import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Workday } from '../model/workday';

@Injectable({
  providedIn: 'root'
})
export class WorkdayService {
  constructor(private http: HttpClient) { }
  public getWorkdays(developerId: number, month: number, year: number): Observable<Workday[]> {
    return this.http.get<Workday[]>(`${environment.backendUrl}/${environment.apiVersion}/workday?developerId=${developerId}&month=${month}&year=${year}`);
  }
  public getMyWorkdays(month: number, year: number): Observable<Workday[]> {
    return this.http.get<Workday[]>(`${environment.backendUrl}/${environment.apiVersion}/workday/me?month=${month}&year=${year}`);
  }
  public postWorkday(workday: Workday): Observable<Workday> {
    return this.http.post<Workday>(`${environment.backendUrl}/${environment.apiVersion}/workday`, workday);
  }
  public patchWorkday(id: number, status: string): Observable<Workday> {
    return this.http.patch<Workday>(`${environment.backendUrl}/${environment.apiVersion}/workday?workdayId=${id}&status=${status}`, {});
  }
  public putWorkday(workday: Workday): Observable<Workday> {
    return this.http.put<Workday>(`${environment.backendUrl}/${environment.apiVersion}/workday`, workday);
  }
  public deleteWorkday(id: number): Observable<Workday> {
    return this.http.delete<Workday>(`${environment.backendUrl}/${environment.apiVersion}/workday?id=${id}`);
  }
}
