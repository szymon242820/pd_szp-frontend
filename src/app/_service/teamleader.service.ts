import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PageResponse } from '../model/page-response';
import { Teamleader } from '../model/teamleader';

@Injectable({
  providedIn: 'root'
})
export class TeamleaderService {
  constructor(private http: HttpClient) { }
  public getTeamleaders(pageNumber: number, pageSize: number): Observable<PageResponse> {
    return this.http.get<PageResponse>(`${environment.backendUrl}/${environment.apiVersion}/teamleader?pageNumber=${pageNumber}&pageSize=${pageSize}`);
  }
  public postTeamleader(teamleader: Teamleader): Observable<Teamleader> {
    return this.http.post<Teamleader>(`${environment.backendUrl}/${environment.apiVersion}/teamleader`, teamleader);
  }
  public putTeamleader(teamleader: Teamleader): Observable<Teamleader> {
    return this.http.put<Teamleader>(`${environment.backendUrl}/${environment.apiVersion}/teamleader`, teamleader);
  }
  public deleteTeamleadern(id: number): Observable<Teamleader> {
    return this.http.delete<Teamleader>(`${environment.backendUrl}/${environment.apiVersion}/teamleader?id=${id}`);
  }
}
