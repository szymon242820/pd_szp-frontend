import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Developer } from '../model/developer';
import { PageResponse } from '../model/page-response';
import { WorkerWithBilling } from '../model/worker-with-billing';
import { WorkerWithSpecifics } from '../model/worker-with-specifics';

@Injectable({
  providedIn: 'root'
})
export class DeveloperService {
  constructor(private http: HttpClient) { }
  public getDevelopers(pageNumber: number, pageSize: number): Observable<PageResponse> {
    return this.http.get<PageResponse>(`${environment.backendUrl}/${environment.apiVersion}/developer?pageNumber=${pageNumber}&pageSize=${pageSize}`);
  }
  public getDevelopersByDevteam(devTeamId: number): Observable<WorkerWithSpecifics[]> {
    return this.http.get<WorkerWithSpecifics[]>(`${environment.backendUrl}/${environment.apiVersion}/developer/devteam/${devTeamId}`);
  }
  public findDeveloperById(developerId: number): Observable<Developer> {
    return this.http.get<Developer>(`${environment.backendUrl}/${environment.apiVersion}/developer/${developerId}`);
  }
  public patchDeveloperDevTeam(developerId: number, devTeamId: number): Observable<Developer> {
    return this.http.patch<Developer>(`${environment.backendUrl}/${environment.apiVersion}/developer?developerId=${developerId}&devTeamId=${devTeamId}`, {});
  }
  public getDevelopersWithBillings(pageNumber: number, pageSize: number): Observable<PageResponse> {
    return this.http.get<PageResponse>(`${environment.backendUrl}/${environment.apiVersion}/developer/billing?pageNumber=${pageNumber}&pageSize=${pageSize}`);
  }
  public findDeveloperWithBillingById(developerId: number): Observable<WorkerWithBilling> {
    return this.http.get<WorkerWithBilling>(`${environment.backendUrl}/${environment.apiVersion}/developer/billing/${developerId}`);
  }
}
