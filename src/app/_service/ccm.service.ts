import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PageResponse } from '../model/page-response';


@Injectable({
  providedIn: 'root'
})
export class CcmService {

  constructor(private http: HttpClient) { }
  public getCcms(pageNumber: number, pageSize: number): Observable<PageResponse> {
    return this.http.get<PageResponse>(`${environment.backendUrl}/${environment.apiVersion}/ccm?pageNumber=${pageNumber}&pageSize=${pageSize}`);
  }
}
