import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Skill } from '../model/skill';

@Injectable({
  providedIn: 'root'
})
export class SkillService {

  constructor(private http: HttpClient) { }
  public getAllSkills(): Observable<Skill[]> {
    return this.http.get<Skill[]>(`${environment.backendUrl}/${environment.apiVersion}/skill`);
  }
}
