import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Devteam } from '../model/devteam';
import { PageResponse } from '../model/page-response';

@Injectable({
  providedIn: 'root'
})
export class DevteamService {
  constructor(private http: HttpClient) { }
  public getDevteams(pageNumber: number, pageSize: number): Observable<PageResponse> {
    return this.http.get<PageResponse>(`${environment.backendUrl}/${environment.apiVersion}/devteam?pageNumber=${pageNumber}&pageSize=${pageSize}`);
  }
  public getAllDevteams(): Observable<Devteam[]> {
    return this.http.get<Devteam[]>(`${environment.backendUrl}/${environment.apiVersion}/devteam/all`);
  }
  public getOwnedDevteam(): Observable<Devteam> {
    return this.http.get<Devteam>(`${environment.backendUrl}/${environment.apiVersion}/devteam/owned`);
  }
}
