import { TestBed } from '@angular/core/testing';

import { DevteamService } from './devteam.service';

describe('DevteamService', () => {
  let service: DevteamService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(DevteamService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
