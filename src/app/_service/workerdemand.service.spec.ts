import { TestBed } from '@angular/core/testing';

import { WorkerdemandService } from './workerdemand.service';

describe('WorkerdemandService', () => {
  let service: WorkerdemandService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(WorkerdemandService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
