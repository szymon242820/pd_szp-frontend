import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { WorkerDemand } from '../model/worker-demand';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class WorkerdemandService {

  constructor(private http: HttpClient) { }
  public getWorkerDemandByStatus(status: string): Observable<WorkerDemand[]> {
    return this.http.get<WorkerDemand[]>(`${environment.backendUrl}/${environment.apiVersion}/demand?status=${status}`);
  }
  public getWorkerDemandById(id: number): Observable<WorkerDemand> {
    return this.http.get<WorkerDemand>(`${environment.backendUrl}/${environment.apiVersion}/demand/${id}`);
  }
  public getMyWorkerDemands(): Observable<WorkerDemand[]> {
    return this.http.get<WorkerDemand[]>(`${environment.backendUrl}/${environment.apiVersion}/demand/me`);
  }
  public putWorkerDemand(workerDemand: WorkerDemand): Observable<WorkerDemand> {
    return this.http.put<WorkerDemand>(`${environment.backendUrl}/${environment.apiVersion}/demand`, workerDemand);
  }
  public postWorkerDemand(workerDemand: WorkerDemand): Observable<WorkerDemand> {
    return this.http.post<WorkerDemand>(`${environment.backendUrl}/${environment.apiVersion}/demand`, workerDemand);
  }
  public patchWorkerDemand(workerDemandId: number, status: string): Observable<WorkerDemand> {
    return this.http.patch<WorkerDemand>(`${environment.backendUrl}/${environment.apiVersion}/demand?workerDemandId=${workerDemandId}&status=${status}`, {});
  }
}
