import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkersActivitiesComponent } from './workers-activities.component';

describe('WorkersActivitiesComponent', () => {
  let component: WorkersActivitiesComponent;
  let fixture: ComponentFixture<WorkersActivitiesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkersActivitiesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(WorkersActivitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
