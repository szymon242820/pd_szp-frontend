import { Component, OnInit } from '@angular/core';
import { DeveloperService } from 'src/app/_service/developer.service';
import { DevteamService } from 'src/app/_service/devteam.service';
import { MatTableDataSource } from '@angular/material/table';
import { HttpErrorResponse } from '@angular/common/http';
import { Devteam } from 'src/app/model/devteam';
import { WorkerWithSpecifics } from 'src/app/model/worker-with-specifics';
import { WorkdayService } from 'src/app/_service/workday.service';
import { Workday } from 'src/app/model/workday';
import { NgToastService } from 'ng-angular-popup';

@Component({
  selector: 'app-workers-activities',
  templateUrl: './workers-activities.component.html',
  styleUrls: ['./workers-activities.component.scss']
})
export class WorkersActivitiesComponent implements OnInit {

  constructor(private workdayService: WorkdayService, private developerService: DeveloperService, private devteamService: DevteamService, private toast: NgToastService) { }
  

  devteam!: Devteam;
  displayedColumns: string[] = ['developer', 'workdays', 'skills', 'email'];
  InnerColumns: string[] = ['date','description','duration','status','actions']
  dataSource = new MatTableDataSource<WorkerWithSpecifics>([]);

  public getDevteam(): void {
    this.devteamService.getOwnedDevteam().subscribe({
      next: (response: Devteam) => {
        this.devteam = response
        this.getData(response.id)
      },
      error: (error: HttpErrorResponse) => alert(error.message)
    })
  }

  public changeStatusWorkday(id: number, status: string): void {
    this.workdayService.patchWorkday(id, status).subscribe({
      next: (response: Workday) => this.toast.success({detail: 'SUCCESS', summary: `Status of workday no. ${response.id} succesfully updated`, duration: 2000}),
      error: (error: HttpErrorResponse) => this.toast.error({detail:'ERROR', summary: error.message, sticky:true}),
    })
  }

  public getData(teamId: number): void {
    this.developerService.getDevelopersByDevteam(teamId).subscribe({
      next: (response: WorkerWithSpecifics[]) => {
        this.dataSource = new MatTableDataSource<WorkerWithSpecifics>(response);
      }
    })
  }

  updateWorkdayStatus(id: number, status: string): void {
    this.changeStatusWorkday(id, status)
    this.getDevteam()
  }

  ngOnInit(): void {
    this.getDevteam()
  }
}