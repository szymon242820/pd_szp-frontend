import { Component, OnInit } from '@angular/core';
import { NgToastService } from 'ng-angular-popup';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { DataService } from 'src/app/_service/data.service';
import { WorkerdemandService } from 'src/app/_service/workerdemand.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-cancel-demand-modal',
  templateUrl: './cancel-demand-modal.component.html',
  styleUrls: ['./cancel-demand-modal.component.scss']
})
export class CancelDemandModalComponent implements OnInit {

  constructor(public cancelModalRef: BsModalRef, private dataService: DataService, private demandService: WorkerdemandService, private toast: NgToastService) {}

  id!: number

  public cancelDemand(id: number): void{
    this.demandService.patchWorkerDemand(id, "CANCELED").subscribe({
      next: () => {
        this.toast.info({detail: 'SUCCESS', summary:'Demand canceled succesfully', duration: 2000})
      },
      error: (error: HttpErrorResponse) => this.toast.error({detail:'ERROR', summary: error.error.message != undefined ? error.error.message : 'Something went wrong!', sticky:true}),
    })
  }

  handleAccept(): void {
    this.cancelDemand(this.id)
    this.cancelModalRef.hide()
  }

  ngOnInit(): void {
    this.id = this.dataService.getDemandId()
  }

}
