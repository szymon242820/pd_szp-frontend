import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CancelDemandModalComponent } from './cancel-demand-modal.component';

describe('CancelDemandModalComponent', () => {
  let component: CancelDemandModalComponent;
  let fixture: ComponentFixture<CancelDemandModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CancelDemandModalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CancelDemandModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
