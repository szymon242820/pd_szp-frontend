import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkerDemandTeamLeaderComponent } from './worker-demand-team-leader.component';

describe('WorkerDemandTeamLeaderComponent', () => {
  let component: WorkerDemandTeamLeaderComponent;
  let fixture: ComponentFixture<WorkerDemandTeamLeaderComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkerDemandTeamLeaderComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(WorkerDemandTeamLeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
