import { Component, OnInit } from '@angular/core';
import { NgToastService } from 'ng-angular-popup';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { WorkerDemand } from 'src/app/model/worker-demand';
import { DataService } from 'src/app/_service/data.service';
import { WorkerdemandService } from 'src/app/_service/workerdemand.service';
import { HttpErrorResponse } from '@angular/common/http';
import { Skill } from 'src/app/model/skill';
import { Specialization } from 'src/app/model/specialization';

@Component({
  selector: 'app-edit-demand-modal',
  templateUrl: './edit-demand-modal.component.html',
  styleUrls: ['./edit-demand-modal.component.scss']
})
export class EditDemandModalComponent implements OnInit {

  constructor(public editModalRef: BsModalRef, private dataService: DataService, private demandService: WorkerdemandService, private toast: NgToastService) { }

  demand: WorkerDemand = {id: 0, exp: '', spec: {id: 0, name: '', description: ''}, skills: [{ id: 0, name: '', description: ''}], status: ''}
  id!: number
  skills: Skill[] = []
  specializations: Specialization[] = []

  handleAccept(): void {
    this.demand.exp = (document.getElementById("experience_edit") as HTMLInputElement).value
    this.demand.spec = <Specialization>this.specializations.find(element => element.id == +(document.getElementById("specialization_edit") as HTMLInputElement).value)
    this.demand.skills = this.getCheckboxVlaues()
    this.putDemand(this.demand)
    this.editModalRef.hide()
  }

  getCheckboxVlaues(): Skill[] {
    var skillsCheckedId: any[] = []
    Array.from(document.getElementsByClassName("check-edit")).forEach((checkbox) => {
      if ((checkbox as HTMLInputElement).checked == true){
        skillsCheckedId.push((checkbox as HTMLInputElement).value)
      }
    })
    var skillsChecked: Skill[] = skillsCheckedId.map(x => <Skill>this.skills.find(element => element.id == x))
    return skillsChecked;
  }

  public putDemand(demand: WorkerDemand): void {
    this.demandService.putWorkerDemand(demand).subscribe({
      next: (response: WorkerDemand) => this.toast.success({detail: 'SUCCESS', summary: 'Information updated', duration: 2000}),
      error: (error: HttpErrorResponse) => this.toast.error({detail:'ERROR', summary: error.message, sticky:true}),
    });
  }

  public getDemandById(id:number): void {
    this.demandService.getWorkerDemandById(id).subscribe({
      next: (response: WorkerDemand) => this.demand = response,
      error: (error: HttpErrorResponse) => this.toast.error({detail:'ERROR', summary: error.message, sticky:true}),
    })
  }

  isHisSkill(skill: Skill, skills: Skill[]): boolean {
    let temp = skills.map(x => x.id)
    return (temp.indexOf(skill.id) !== -1)
  }

  ngOnInit(): void {
    this.id = this.dataService.getDemandId()
    this.getDemandById(this.id)
    this.skills = this.dataService.getSkills()
    this.specializations = this.dataService.getSpecializations()
  }

}
