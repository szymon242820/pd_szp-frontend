import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import { NgToastService } from 'ng-angular-popup';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Skill } from 'src/app/model/skill';
import { Specialization } from 'src/app/model/specialization';
import { DataService } from 'src/app/_service/data.service';
import { SkillService } from 'src/app/_service/skill.service';
import { SpecializationService } from 'src/app/_service/specialization.service';
import { WorkerdemandService } from 'src/app/_service/workerdemand.service';
import { WorkerDemand } from '../../model/worker-demand';
import { CancelDemandModalComponent } from './cancel-demand-modal/cancel-demand-modal.component';
import { EditDemandModalComponent } from './edit-demand-modal/edit-demand-modal.component';

@Component({
  selector: 'app-worker-demand-team-leader',
  templateUrl: './worker-demand-team-leader.component.html',
  styleUrls: ['./worker-demand-team-leader.component.scss']
})
export class WorkerDemandTeamLeaderComponent implements OnInit, OnDestroy {

  constructor(private demandService: WorkerdemandService, private skillService: SkillService, private specService: SpecializationService, private toast: NgToastService, private router: Router, private dataService: DataService, private modalService: BsModalService) { }
  displayedColumns: string[] = ['id', 'info', 'skills', 'status', 'action'];
  dataSource = new MatTableDataSource<WorkerDemand>([]);
  skills: Skill[] = []
  specializations: Specialization[] = []
  editModalRef!: BsModalRef;
  cancelModalRef!: BsModalRef;
  refresh: any

  handleCreate(): void {
    let demand: WorkerDemand = <WorkerDemand>{}
    demand.exp = (document.getElementById("experience") as HTMLInputElement).value
    demand.spec = <Specialization>this.specializations.find(element => element.id == +(document.getElementById("specialization") as HTMLInputElement).value)
    demand.skills = this.getCheckboxVlaues()
    this.postDemand(demand)
    this.getMyDemands()
  }

  openCancelModal(id: number): void {
    this.dataService.setDemandId(id)
    this.cancelModalRef = this.modalService.show(CancelDemandModalComponent);
    this.cancelModalRef.onHide?.subscribe(()=> {
      this.getMyDemands()
    })
  }

  openEditModal(id: number): void {
    this.dataService.setDemandId(id)
    this.dataService.setSkills(this.skills)
    this.dataService.setSpecializations(this.specializations)
    this.editModalRef = this.modalService.show(EditDemandModalComponent);
    this.editModalRef.onHide?.subscribe(()=> {
      this.getMyDemands()
    })
  }

  getCheckboxVlaues(): Skill[] {
    var skillsCheckedId: any[] = []
    Array.from(document.getElementsByClassName("form-check-input")).forEach((checkbox) => {
      if ((checkbox as HTMLInputElement).checked == true){
        skillsCheckedId.push((checkbox as HTMLInputElement).value)
      }
    })
    var skillsChecked: Skill[] = skillsCheckedId.map(x => <Skill>this.skills.find(element => element.id == x))
    return skillsChecked;
  }

  public getMyDemands(): void {
    this.demandService.getMyWorkerDemands().subscribe({
      next: (response: WorkerDemand[]) => {
        this.dataSource = new MatTableDataSource<WorkerDemand>(response);
      },
      error: (error: HttpErrorResponse) => this.toast.error({detail:'ERROR', summary: error.error.message != undefined ? error.error.message : 'Something went wrong!', sticky:true}),
    })
  }

  public postDemand(demand: WorkerDemand): void {
    this.demandService.postWorkerDemand(demand).subscribe({
      next: (response: WorkerDemand) => {
        this.toast.success({detail: 'SUCCESS', summary:'Information added', duration: 2000})
      },
      error: (error: HttpErrorResponse) => this.toast.error({detail:'ERROR', summary: error.error.message != undefined ? error.error.message : 'Something went wrong!', sticky:true}),
    })
  }

  public getAllSkills(): void {
    this.skillService.getAllSkills().subscribe({
      next: (response: Skill[]) => {
        this.skills = response
      },
      error: (error: HttpErrorResponse) => this.toast.error({detail:'ERROR', summary: error.error.message != undefined ? error.error.message : 'Something went wrong!', sticky:true}),
    })
  }

  public getAllSpecializations(): void {
    this.specService.getAllSpecializations().subscribe({
      next: (response: Specialization[]) => {
        this.specializations = response
      },
      error: (error: HttpErrorResponse) => this.toast.error({detail:'ERROR', summary: error.error.message != undefined ? error.error.message : 'Something went wrong!', sticky:true}),
    })
  }

  requiredInputs(): boolean {
    let spec = <Specialization>this.specializations.find(element => element.id == +(document.getElementById("specialization") as HTMLInputElement).value)
    let exp = (document.getElementById("experience") as HTMLInputElement).value
    if (spec != undefined && exp != ''){
      return true
    }
    return false
  }

  ngOnInit(): void {
    this.getMyDemands()
    this.getAllSkills()
    this.getAllSpecializations()
    this.refresh = setInterval(() => this.getMyDemands(), 3000)
  }

  ngOnDestroy(): void {
    clearInterval(this.refresh)
  }

}
