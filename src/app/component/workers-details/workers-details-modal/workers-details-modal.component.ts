import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgToastService } from 'ng-angular-popup';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { WorkerWithBilling } from 'src/app/model/worker-with-billing';
import { DataService } from 'src/app/_service/data.service';
import { DeveloperService } from 'src/app/_service/developer.service';
import { DevteamService } from 'src/app/_service/devteam.service';


@Component({
  selector: 'app-workers-details-modal',
  templateUrl: './workers-details-modal.component.html',
  styleUrls: ['./workers-details-modal.component.scss']
})
export class WorkersDetailsModalComponent implements OnInit {

  constructor(public showWorkerDetailsModalRef: BsModalRef, private devService: DeveloperService, private devteamService: DevteamService, private dataService: DataService, private developerService: DeveloperService, private toast: NgToastService) { }
  worker: WorkerWithBilling = {id: 0, name: "", surname: "", email: "", exp: "", spec: {id: 0, name: "", description: ""}, skills: [{id: 0, name: "", description: ""}], devTeam: {id: 0, name: "", teamLeader: {id: 0, name: "", surname: "", email: "", ccm: {id: 0, name: "", surname: "", email: ""}}}, createdAt: '', bankAccNumber: '', salaryPerHour: 0}


  public findDeveloperById(id: number): void{
    this.devService.findDeveloperWithBillingById(id).subscribe({
      next: (response: WorkerWithBilling) => {
        this.worker = response
      },
      error: (error: HttpErrorResponse) => this.toast.error({detail: 'ERROR', summary: error.message, sticky: true})
    })
  }

  ngOnInit(): void {
    this.findDeveloperById(this.dataService.getWorkerId())
  }

}
