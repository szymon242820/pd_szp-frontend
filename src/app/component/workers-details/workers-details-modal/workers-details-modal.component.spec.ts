import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkersDetailsModalComponent } from './workers-details-modal.component';

describe('WorkersDetailsModalComponent', () => {
  let component: WorkersDetailsModalComponent;
  let fixture: ComponentFixture<WorkersDetailsModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkersDetailsModalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(WorkersDetailsModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
