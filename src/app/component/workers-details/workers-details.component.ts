import { HttpErrorResponse } from '@angular/common/http';
import { ViewChild, Component, OnInit } from '@angular/core';
import { PageResponse } from 'src/app/model/page-response';
import { DeveloperService } from 'src/app/_service/developer.service';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { DataService } from 'src/app/_service/data.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { WorkersDetailsModalComponent } from './workers-details-modal/workers-details-modal.component';
import { NgToastService } from 'ng-angular-popup';
import { WorkerWithBilling } from 'src/app/model/worker-with-billing';


@Component({
  selector: 'app-workers-details',
  templateUrl: './workers-details.component.html',
  styleUrls: ['./workers-details.component.scss']
})
export class WorkersDetailsComponent implements OnInit {

  constructor(private devService: DeveloperService, private dataService: DataService, private modalService: BsModalService, private toast: NgToastService) { }
  pageSize = 10;
  pageSizeOptions = [10, 25, 50];
  pageIndex = 0;
  length = 0;
  showWorkerDetailsModalRef!: BsModalRef;
  pageEvent?: PageEvent;
  displayedColumns: string[] = ['joindate', 'name', 'surname', 'email', 'action'];
  dataSource = new MatTableDataSource<WorkerWithBilling>([]);
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  openWorkerDetailsModal(workerId: number): void {
    this.dataService.setWorkerId(workerId)
    this.showWorkerDetailsModalRef = this.modalService.show(WorkersDetailsModalComponent);
  }

  public getTableData(pageNumber: number, pageSize: number): void {
    this.devService.getDevelopersWithBillings(pageNumber, pageSize).subscribe({
      next: (response: PageResponse) => {
        this.dataSource.data = response.result;
        this.pageIndex = response.pageNumber;
        this.length = response.totalElements;
      },
      error: (error: HttpErrorResponse) => this.toast.error({detail: 'ERROR', summary: error.message, sticky: true}),
    })
  }

  onPageChange(event: PageEvent): void {
    this.getTableData(event.pageIndex, event.pageSize);
  }

  ngOnInit(): void {
    this.getTableData(0, 10)
  }

}
