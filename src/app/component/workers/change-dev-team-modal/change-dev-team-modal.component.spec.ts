import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeDevTeamModalComponent } from './change-dev-team-modal.component';

describe('ChangeDevTeamModalComponent', () => {
  let component: ChangeDevTeamModalComponent;
  let fixture: ComponentFixture<ChangeDevTeamModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ChangeDevTeamModalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ChangeDevTeamModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
