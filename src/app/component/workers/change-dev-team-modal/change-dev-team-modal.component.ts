import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgToastService } from 'ng-angular-popup';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Developer } from 'src/app/model/developer';
import { Devteam } from 'src/app/model/devteam';
import { DataService } from 'src/app/_service/data.service';
import { DeveloperService } from 'src/app/_service/developer.service';
import { DevteamService } from 'src/app/_service/devteam.service';

@Component({
  selector: 'app-change-dev-team-modal',
  templateUrl: './change-dev-team-modal.component.html',
  styleUrls: ['./change-dev-team-modal.component.scss']
})
export class ChangeDevTeamModalComponent implements OnInit {

  worker: Developer = {id: 0, name: "", surname: "", email: "", exp: "", spec: {id: 0, name: "", description: ""}, skills: [{id: 0, name: "", description: ""}], devTeam: {id: 0, name: "", teamLeader: {id: 0, name: "", surname: "", email: "", ccm: {id: 0, name: "", surname: "", email: ""}}}}
  devteams: Devteam[] = []

  constructor(public editDevTeamModalRef: BsModalRef, private devService: DeveloperService, private devteamService: DevteamService, private dataService: DataService, private developerService: DeveloperService, private toast: NgToastService) {}

  public getDevteams(): void {
    this.devteamService.getAllDevteams().subscribe({
      next: (response: Devteam[]) => {
        this.devteams = response
      },
      error: (error: HttpErrorResponse) => this.toast.error({detail:'ERROR', summary: error.error.message != undefined ? error.error.message : 'Something went wrong!', sticky:true}),
    })
  }
  
  public findDeveloperById(id: number): void{
    this.devService.findDeveloperById(id).subscribe({
      next: (response: Developer) => {
        this.worker = response
      },
      error: (error: HttpErrorResponse) => this.toast.error({detail: 'ERROR', summary: error.message, sticky: true})
    })
  }

  public patchDeveloperDevteam(developerId: number, devteamId: number): void {
    this.developerService.patchDeveloperDevTeam(developerId, devteamId).subscribe({
      next: (response: Developer) => {
        this.toast.success({detail: 'SUCCESS', summary:'Devteam changed succesfully', duration: 2000})
      },
      error: (error: HttpErrorResponse) => this.toast.error({detail: 'ERROR', summary: error.message, sticky: true}),
    })
  }

  handleAccept(): void {
    let devteamId = parseInt((document.getElementById('devteam') as HTMLInputElement).value)
    this.patchDeveloperDevteam(this.worker.id, devteamId)
    this.editDevTeamModalRef.hide()
  }

  ngOnInit(): void {
    this.findDeveloperById(this.dataService.getWorkerId())
    this.getDevteams()
  }

}
