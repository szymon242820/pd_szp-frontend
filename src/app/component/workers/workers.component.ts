import { HttpErrorResponse } from '@angular/common/http';
import { ViewChild, Component, OnInit, OnDestroy } from '@angular/core';
import { Developer } from 'src/app/model/developer';
import { PageResponse } from 'src/app/model/page-response';
import { DeveloperService } from 'src/app/_service/developer.service';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { MatTableDataSource } from '@angular/material/table';
import { DataService } from 'src/app/_service/data.service';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { ChangeDevTeamModalComponent } from './change-dev-team-modal/change-dev-team-modal.component';
import { NgToastService } from 'ng-angular-popup';

@Component({
  selector: 'app-workers',
  templateUrl: './workers.component.html',
  styleUrls: ['./workers.component.scss']
})
export class WorkersComponent implements OnInit, OnDestroy {

  constructor(private devService: DeveloperService, private dataService: DataService, private modalService: BsModalService, private toast: NgToastService) { }
  pageSize = 10;
  pageSizeOptions = [10, 25, 50];
  pageIndex = 0;
  length = 0;
  editDevTeamModalRef!: BsModalRef;
  refresh: any;

  pageEvent?: PageEvent;

  displayedColumns: string[] = ['id', 'name', 'surname', 'email', 'exp', 'spec', 'skills', 'devTeam', 'action'];
  dataSource = new MatTableDataSource<Developer>([]);
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  openEditDevTeamModal(id: number): void {
    this.dataService.setWorkerId(id)
    this.editDevTeamModalRef = this.modalService.show(ChangeDevTeamModalComponent);
    this.editDevTeamModalRef.onHide?.subscribe(()=> {
      this.getTableData(this.pageIndex, this.pageSize);
    })
  }

  public getTableData(pageNumber: number, pageSize: number): void {
    this.devService.getDevelopers(pageNumber, pageSize).subscribe({
      next: (response: PageResponse) => {
        this.dataSource.data = response.result;
        this.pageIndex = response.pageNumber;
        this.length = response.totalElements;
      },
      error: (error: HttpErrorResponse) => this.toast.error({detail: 'ERROR', summary: error.message, sticky: true}),
    })
  }

  onPageChange(event: PageEvent): void {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.getTableData(event.pageIndex, event.pageSize);
  }

  ngOnInit(): void {
    this.getTableData(this.pageIndex, this.pageSize)
    this.refresh = setInterval(() => this.getTableData(this.pageIndex, this.pageSize), 3000)
  }

  ngOnDestroy(): void {
    clearInterval(this.refresh)
  }

}
