import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgToastService } from 'ng-angular-popup';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Joboffer } from 'src/app/model/joboffer';
import { Skill } from 'src/app/model/skill';
import { Specialization } from 'src/app/model/specialization';
import { DataService } from 'src/app/_service/data.service';
import { JobofferService } from 'src/app/_service/joboffer.service';

@Component({
  selector: 'app-offer-edit-modal',
  templateUrl: './offer-edit-modal.component.html',
  styleUrls: ['./offer-edit-modal.component.scss']
})
export class OfferEditModalComponent implements OnInit {

  constructor(public editOfferModalRef: BsModalRef, private dataService: DataService, private jobOfferService: JobofferService, private toast: NgToastService) { }

  joboffer: Joboffer = {id: 0, description: '', exp: '', spec: {id: 0, name: '', description: ''}, skills: [{id: 0,  name: '', description: ''}]}
  id!: number
  skills: Skill[] = []
  specializations: Specialization[] = []

  handleAccept(): void {
    this.joboffer.exp = (document.getElementById("experience_edit") as HTMLInputElement).value
    this.joboffer.spec = <Specialization>this.specializations.find(element => element.id == +(document.getElementById("specialization_edit") as HTMLInputElement).value)
    this.joboffer.description = (document.getElementById("description_edit") as HTMLInputElement).value
    this.joboffer.skills = this.getCheckboxVlaues()
    this.putJobOffer(this.joboffer)
    this.editOfferModalRef.hide()
  }

  getCheckboxVlaues(): Skill[] {
    var skillsCheckedId: any[] = []
    Array.from(document.getElementsByClassName("check-edit")).forEach((checkbox) => {
      if ((checkbox as HTMLInputElement).checked == true){
        skillsCheckedId.push((checkbox as HTMLInputElement).value)
      }
    })
    var skillsChecked: Skill[] = skillsCheckedId.map(x => <Skill>this.skills.find(element => element.id == x))
    return skillsChecked;
  }

  public getJobOfferById(id: number): void {
    this.jobOfferService.getJobofferById(id).subscribe({
      next: (response: Joboffer) => this.joboffer = response,
      error: (error: HttpErrorResponse) => this.toast.error({detail:'ERROR', summary: error.message, sticky:true}),
    })
  }

  public putJobOffer(jobOffer: Joboffer): void {
    this.jobOfferService.putJoboffer(jobOffer).subscribe({
      next: (response: Joboffer) => this.toast.success({detail: 'SUCCESS', summary: 'Information updated', duration: 2000}),
      error: (error: HttpErrorResponse) => this.toast.error({detail:'ERROR', summary: error.message, sticky:true}),
    });
  }

  shouldBeChecked(skill: Skill, skills: Skill[]): boolean {
    let temp = skills.map(x => x.id)
    return (temp.indexOf(skill.id) !== -1)
  }

  ngOnInit(): void {
    this.id = this.dataService.getJobOfferId()
    this.getJobOfferById(this.id)
    this.skills = this.dataService.getSkills()
    this.specializations = this.dataService.getSpecializations()
  }

}
