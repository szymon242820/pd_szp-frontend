import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgToastService } from 'ng-angular-popup';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { DataService } from 'src/app/_service/data.service';
import { JobofferService } from 'src/app/_service/joboffer.service';

@Component({
  selector: 'app-offer-delete-modal',
  templateUrl: './offer-delete-modal.component.html',
  styleUrls: ['./offer-delete-modal.component.scss']
})
export class OfferDeleteModalComponent implements OnInit {

  constructor(public deleteOfferModalRef: BsModalRef, private dataService: DataService, private jobOfferService: JobofferService, private toast: NgToastService) { }

  id!: number

  public deleteOffer(id: number): void{
    this.jobOfferService.deletJoboffer(id).subscribe({
      next: () => {
        this.toast.info({detail: 'SUCCESS', summary:'Information deleted', duration: 2000})
      },
      error: (error: HttpErrorResponse) => this.toast.error({detail:'ERROR', summary: error.error.message != undefined ? error.error.message : 'Something went wrong!', sticky:true}),
    })
  }

  handleAccept(): void {
    this.deleteOffer(this.id)
    this.deleteOfferModalRef.hide()
  }

  ngOnInit(): void {
    this.id = this.dataService.getJobOfferId()
  }

}
