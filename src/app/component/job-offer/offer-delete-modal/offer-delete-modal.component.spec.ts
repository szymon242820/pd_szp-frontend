import { ComponentFixture, TestBed } from '@angular/core/testing';

import { OfferDeleteModalComponent } from './offer-delete-modal.component';

describe('OfferDeleteModalComponent', () => {
  let component: OfferDeleteModalComponent;
  let fixture: ComponentFixture<OfferDeleteModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ OfferDeleteModalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(OfferDeleteModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
