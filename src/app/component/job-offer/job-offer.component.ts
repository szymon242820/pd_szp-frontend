import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgToastService } from 'ng-angular-popup';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { Joboffer } from 'src/app/model/joboffer';
import { PageResponse } from 'src/app/model/page-response';
import { MatPaginator, PageEvent } from '@angular/material/paginator';
import { Skill } from 'src/app/model/skill';
import { Specialization } from 'src/app/model/specialization';
import { DataService } from 'src/app/_service/data.service';
import { JobofferService } from 'src/app/_service/joboffer.service';
import { SkillService } from 'src/app/_service/skill.service';
import { SpecializationService } from 'src/app/_service/specialization.service';
import { OfferDeleteModalComponent } from './offer-delete-modal/offer-delete-modal.component';
import { OfferEditModalComponent } from './offer-edit-modal/offer-edit-modal.component';
import { MatTableDataSource } from '@angular/material/table';


@Component({
  selector: 'app-job-offer',
  templateUrl: './job-offer.component.html',
  styleUrls: ['./job-offer.component.scss']
})
export class JobOfferComponent implements OnInit, OnDestroy {

  constructor(private jobofferService: JobofferService, private skillService: SkillService, private specService: SpecializationService, private toast: NgToastService, private dataService: DataService, private modalService: BsModalService) { }

  pageSize = 10;
  pageSizeOptions = [10, 25, 50];
  pageIndex = 0;
  length = 0;
  pageEvent?: PageEvent;

  displayedColumns: string[] = ['id', 'info', 'skills', 'description', 'action'];
  dataSource = new MatTableDataSource<Joboffer>([]);
  @ViewChild(MatPaginator) paginator!: MatPaginator;

  skills: Skill[] = [];
  specializations: Specialization[] = [];
  editOfferModalRef!: BsModalRef;
  deleteOfferModalRef!: BsModalRef;
  refresh: any;

  handleCreate(): void {
    let offer: Joboffer = <Joboffer>{}
    offer.description = (document.getElementById("description") as HTMLInputElement).value
    offer.exp = (document.getElementById("experience") as HTMLInputElement).value
    offer.spec = <Specialization>this.specializations.find(element => element.id == +(document.getElementById("specialization") as HTMLInputElement).value)
    offer.skills = this.getCheckboxVlaues()
    this.postOffer(offer)
    this.getJoboffers(this.pageIndex, this.pageSize)
  }

  openDeleteModal(id: number): void {
    this.dataService.setJobOfferId(id)
    this.deleteOfferModalRef = this.modalService.show(OfferDeleteModalComponent);
    this.deleteOfferModalRef.onHide?.subscribe(()=> {
      this.getJoboffers(this.pageIndex, this.pageSize)
    })
  }

  openEditModal(id: number): void {
    this.dataService.setJobOfferId(id)
    this.dataService.setSkills(this.skills)
    this.dataService.setSpecializations(this.specializations)
    this.editOfferModalRef = this.modalService.show(OfferEditModalComponent);
    this.editOfferModalRef.onHide?.subscribe(()=> {
      this.getJoboffers(this.pageIndex, this.pageSize)
    })
  }

  getCheckboxVlaues(): Skill[] {
    var skillsCheckedId: any[] = []
    Array.from(document.getElementsByClassName("form-check-input")).forEach((checkbox) => {
      if ((checkbox as HTMLInputElement).checked == true){
        skillsCheckedId.push((checkbox as HTMLInputElement).value)
      }
    })
    var skillsChecked: Skill[] = skillsCheckedId.map(x => <Skill>this.skills.find(element => element.id == x))
    return skillsChecked;
  }

  public getJoboffers(pageNumber: number, pageSize: number): void {
    this.jobofferService.getJoboffers(pageNumber, pageSize).subscribe({
      next: (response: PageResponse) => { 
        this.dataSource.data = response.result;
        this.pageIndex = response.pageNumber;
        this.length = response.totalElements;
      },
      error: (error: HttpErrorResponse) => this.toast.error({detail:'ERROR', summary: error.error.message != undefined ? error.error.message : 'Something went wrong!', sticky:true}),
    })
  }

  public postOffer(joboffer: Joboffer): void {
    this.jobofferService.postJoboffer(joboffer).subscribe({
      next: (response: Joboffer) => {
        this.toast.success({detail: 'SUCCESS', summary:'Information added', duration: 2000})
      },
      error: (error: HttpErrorResponse) => this.toast.error({detail:'ERROR', summary: error.error.message != undefined ? error.error.message : 'Something went wrong!', sticky:true}),
    })
  }

  public getAllSkills(): void {
    this.skillService.getAllSkills().subscribe({
      next: (response: Skill[]) => {
        this.skills = response
      },
      error: (error: HttpErrorResponse) => this.toast.error({detail:'ERROR', summary: error.error.message != undefined ? error.error.message : 'Something went wrong!', sticky:true}),
    })
  }

  public getAllSpecializations(): void {
    this.specService.getAllSpecializations().subscribe({
      next: (response: Specialization[]) => {
        this.specializations = response
      },
      error: (error: HttpErrorResponse) => this.toast.error({detail:'ERROR', summary: error.error.message != undefined ? error.error.message : 'Something went wrong!', sticky:true}),
    })
  }

  requiredInputs(): boolean {
    let spec = <Specialization>this.specializations.find(element => element.id == +(document.getElementById("specialization") as HTMLInputElement).value)
    let exp = (document.getElementById("experience") as HTMLInputElement).value
    let desc = (document.getElementById("description") as HTMLInputElement).value
    if (spec != undefined && exp != '' && desc != ''){
      return false
    }
    return true
  }

  onPageChange(event: PageEvent): void {
    this.pageIndex = event.pageIndex
    this.pageSize = event.pageSize
    this.getJoboffers(event.pageIndex, event.pageSize);
  }

  ngOnInit(): void {
    this.getJoboffers(this.pageIndex, this.pageSize)
    this.getAllSkills()
    this.getAllSpecializations()
    this.refresh = setInterval(() => this.getJoboffers(this.pageIndex, this.pageSize), 3000)
  }

  ngOnDestroy(): void {
    clearInterval(this.refresh)
  }

}
