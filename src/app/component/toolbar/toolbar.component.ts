import { Component, OnInit } from '@angular/core';
import { KeycloakService } from 'keycloak-angular';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.scss']
})
export class ToolbarComponent implements OnInit {

  constructor(private keycloak: KeycloakService, public router: Router) { }

  username: string|undefined
  roles: Array<string>|undefined
  isLoggedIn = false

  checkUserRole(role: string): boolean {
    if(this.roles?.indexOf(role) != -1){
      return true
    }
    return false
  }

  logout(): void {
    this.keycloak.logout(environment.frontendUrl)
    sessionStorage.setItem('isLoggedIn', 'false')
  }
  
  login(): void {
    this.keycloak.login()
  }

  ngOnInit(): void {
    this.keycloak.isLoggedIn().then(loggedIn => {
      if (loggedIn) {
        this.username = this.keycloak.getUsername()
        this.roles = this.keycloak.getUserRoles()
        this.isLoggedIn = true
        sessionStorage.setItem('isLoggedIn', this.isLoggedIn.toString())
        sessionStorage.setItem('roles', this.roles.toString())
      } else {
        this.isLoggedIn = false
      }
    })
  }
}