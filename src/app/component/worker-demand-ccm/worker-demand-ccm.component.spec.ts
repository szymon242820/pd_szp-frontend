import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkerDemandCcmComponent } from './worker-demand-ccm.component';

describe('WorkerDemandCcmComponent', () => {
  let component: WorkerDemandCcmComponent;
  let fixture: ComponentFixture<WorkerDemandCcmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkerDemandCcmComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(WorkerDemandCcmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
