import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { NgToastService } from 'ng-angular-popup';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { WorkerDemand } from 'src/app/model/worker-demand';
import { DataService } from 'src/app/_service/data.service';
import { WorkerdemandService } from 'src/app/_service/workerdemand.service';

import { DemandChangeStatusCcmModalComponent } from './demand-change-status-ccm-modal/demand-change-status-ccm-modal.component';

@Component({
  selector: 'app-worker-demand-ccm',
  templateUrl: './worker-demand-ccm.component.html',
  styleUrls: ['./worker-demand-ccm.component.scss']
})
export class WorkerDemandCcmComponent implements OnInit, OnDestroy {

  constructor(private demandService: WorkerdemandService, private toast: NgToastService, private dataService: DataService, private modalService: BsModalService) { }

  demandStatusCcmModalRef!: BsModalRef;
  refresh: any;
  displayedColumns: string[] = ['id', 'spec', 'exp', 'skills', 'status', 'action'];
  dataSourceNew = new MatTableDataSource<WorkerDemand>([]);
  dataSourceCcmAccepted = new MatTableDataSource<WorkerDemand>([]);
  dataSourceInProgress = new MatTableDataSource<WorkerDemand>([]);

  public getDemands(): void {
    this.demandService.getWorkerDemandByStatus('NEW').subscribe({
      next: (response1: WorkerDemand[]) => {
        this.dataSourceNew = new MatTableDataSource<WorkerDemand>(response1);
          this.demandService.getWorkerDemandByStatus('IN_PROGRESS').subscribe({
            next: (response2: WorkerDemand[]) => {
              this.dataSourceInProgress = new MatTableDataSource<WorkerDemand>(response2);
              this.demandService.getWorkerDemandByStatus('CCM_ACCEPTED').subscribe({
                next: (response3: WorkerDemand[]) => {
                  this.dataSourceCcmAccepted = new MatTableDataSource<WorkerDemand>(response3);
                },
                error: (error: HttpErrorResponse) => this.toast.error({detail:'ERROR', summary: error.error.message != undefined ? error.error.message : 'Something went wrong!', sticky:true}),
              })
            },
            error: (error: HttpErrorResponse) => this.toast.error({detail:'ERROR', summary: error.error.message != undefined ? error.error.message : 'Something went wrong!', sticky:true}),
          })
        },
      error: (error: HttpErrorResponse) => this.toast.error({detail:'ERROR', summary: error.error.message != undefined ? error.error.message : 'Something went wrong!', sticky:true}),
    })
  }

  openChangeStatusModal(id: number): void {
    this.dataService.setDemandId(id)
    this.demandStatusCcmModalRef = this.modalService.show(DemandChangeStatusCcmModalComponent);
    this.demandStatusCcmModalRef.onHide?.subscribe(()=> {
      this.getDemands()
    })
  }

  ngOnInit(): void {
    this.getDemands()
    this.refresh = setInterval(() => this.getDemands(), 3000)
  }
  ngOnDestroy(): void {
    clearInterval(this.refresh)
  }

}
