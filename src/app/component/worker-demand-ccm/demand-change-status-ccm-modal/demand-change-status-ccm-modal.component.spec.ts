import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DemandChangeStatusCcmModalComponent } from './demand-change-status-ccm-modal.component';

describe('DemandChangeStatusCcmModalComponent', () => {
  let component: DemandChangeStatusCcmModalComponent;
  let fixture: ComponentFixture<DemandChangeStatusCcmModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DemandChangeStatusCcmModalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DemandChangeStatusCcmModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
