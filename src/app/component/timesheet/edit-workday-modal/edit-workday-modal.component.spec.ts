import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditWorkdayModalComponent } from './edit-workday-modal.component';

describe('EditWorkdayModalComponent', () => {
  let component: EditWorkdayModalComponent;
  let fixture: ComponentFixture<EditWorkdayModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditWorkdayModalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EditWorkdayModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
