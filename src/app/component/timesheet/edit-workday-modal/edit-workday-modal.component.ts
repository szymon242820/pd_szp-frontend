import { Component, OnInit } from '@angular/core';
import { NgToastService } from 'ng-angular-popup';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Workday } from 'src/app/model/workday';
import { DataService } from 'src/app/_service/data.service';
import { WorkdayService } from 'src/app/_service/workday.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-edit-workday-modal',
  templateUrl: './edit-workday-modal.component.html',
  styleUrls: ['./edit-workday-modal.component.scss']
})
export class EditWorkdayModalComponent implements OnInit {

  constructor(public editModalRef: BsModalRef, private dataService: DataService, private workdayService: WorkdayService, private toast: NgToastService) {}

  workday!: Workday

  public putWorkday(workday: Workday): void {
    this.workdayService.putWorkday(workday).subscribe({
      next: (response: Workday) => {
        this.toast.success({detail: 'SUCCESS', summary: 'Information updated', duration: 2000})
      },
      error: (error: HttpErrorResponse) => this.toast.error({detail:'ERROR', summary: error.error.message != undefined ? error.error.message : 'Something went wrong!', sticky:true}),
    });
  }

  handleAccept(): void {
    this.workday.description = (document.getElementById('desc') as HTMLInputElement).value
    this.workday.duration.hours = Number((document.getElementById('hours') as HTMLInputElement).value)
    this.workday.duration.minutes = Number((document.getElementById('minutes') as HTMLInputElement).value)
    this.putWorkday(this.workday)
    this.editModalRef.hide()
  }

  ngOnInit(): void {
    this.workday = this.dataService.getWorkday()
  }

}
