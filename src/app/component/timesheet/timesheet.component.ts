import { Component, OnInit } from '@angular/core';
import { Workday } from 'src/app/model/workday';
import { WorkdayService } from 'src/app/_service/workday.service';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { AddWorkdayModalComponent } from './add-workday-modal/add-workday-modal.component';
import { DataService } from 'src/app/_service/data.service';
import { EditWorkdayModalComponent } from './edit-workday-modal/edit-workday-modal.component';
import { NgToastService } from 'ng-angular-popup';
import { DeleteWorkdayModalComponent } from './delete-workday-modal/delete-workday-modal.component';
import { MatTableDataSource } from '@angular/material/table';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-timesheet',
  templateUrl: './timesheet.component.html',
  styleUrls: ['./timesheet.component.scss']
})
export class TimesheetComponent implements OnInit {
  
  constructor(private workdayService: WorkdayService, private modalService: BsModalService, private dataService: DataService, private toast: NgToastService) { }
  displayedColumns: string[] = ['date', 'description', 'duration', 'status', 'action'];
  dataSource = new MatTableDataSource<Workday>([]);
  selected!: Date;
  addModalRef!: BsModalRef;
  editModalRef!: BsModalRef;
  deleteModalRef!: BsModalRef;

  openAddModal(): void {
    this.dataService.setDate(this.selected!)
    this.addModalRef = this.modalService.show(AddWorkdayModalComponent);
    this.addModalRef.onHide?.subscribe(()=> {
      this.getMyWorkdays(this.selected.getMonth()+1, this.selected.getFullYear())
    })
  }

  openEditModal(id: number): void {
    let workday: Workday = this.dataSource.data.find(wd => wd.id === id)!
    this.dataService.setWorkday(workday)
    this.editModalRef = this.modalService.show(EditWorkdayModalComponent);
    this.editModalRef.onHide?.subscribe(()=> {
      this.getMyWorkdays(this.selected.getMonth()+1, this.selected.getFullYear())
    })
  }

  openDeleteModal(id: number): void {
    this.dataService.setWorkdayId(id)
    this.deleteModalRef = this.modalService.show(DeleteWorkdayModalComponent);
    this.deleteModalRef.onHide?.subscribe(()=> {
      this.getMyWorkdays(this.selected.getMonth()+1, this.selected.getFullYear())
    })
  }
  
  public getMyWorkdays(month: number, year: number): void {
    this.workdayService.getMyWorkdays(month, year).subscribe({
      next: (response: Workday[]) => {
        this.dataSource = new MatTableDataSource<Workday>(response);
      },
      error: (error: HttpErrorResponse) => this.toast.error({detail:'ERROR', summary: error.error.message != undefined ? error.error.message : 'Something went wrong!', sticky:true}),
    })
  }

  ngOnInit(): void {
    let date = new Date(Date.now())
    this.getMyWorkdays(date.getMonth()+1, date.getFullYear())
  }
}