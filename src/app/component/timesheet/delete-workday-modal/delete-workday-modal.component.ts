import { Component, OnInit } from '@angular/core';
import { NgToastService } from 'ng-angular-popup';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { DataService } from 'src/app/_service/data.service';
import { WorkdayService } from 'src/app/_service/workday.service';
import { HttpErrorResponse } from '@angular/common/http';

@Component({
  selector: 'app-delete-workday-modal',
  templateUrl: './delete-workday-modal.component.html',
  styleUrls: ['./delete-workday-modal.component.scss']
})
export class DeleteWorkdayModalComponent implements OnInit {

  constructor(public deleteModalRef: BsModalRef, private dataService: DataService, private workdayService: WorkdayService, private toast: NgToastService) {}

  id!: number

  public deleteWorkday(id: number): void{
    this.workdayService.deleteWorkday(id).subscribe({
      next: () => {
        this.toast.info({detail: 'SUCCESS', summary:'Information deleted', duration: 2000})
      },
      error: (error: HttpErrorResponse) => this.toast.error({detail:'ERROR', summary: error.error.message != undefined ? error.error.message : 'Something went wrong!', sticky:true}),
    })
  }

  handleAccept(): void{
    this.deleteWorkday(this.id)
    this.deleteModalRef.hide()
  }

  ngOnInit(): void {
    this.id = this.dataService.getWorkdayId()
  }

}
