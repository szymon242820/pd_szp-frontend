import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DeleteWorkdayModalComponent } from './delete-workday-modal.component';

describe('DeleteWorkdayModalComponent', () => {
  let component: DeleteWorkdayModalComponent;
  let fixture: ComponentFixture<DeleteWorkdayModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DeleteWorkdayModalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DeleteWorkdayModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
