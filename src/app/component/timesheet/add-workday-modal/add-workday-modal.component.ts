import { Component, OnInit } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { DataService } from 'src/app/_service/data.service';
import { WorkdayService } from 'src/app/_service/workday.service';
import { Workday } from 'src/app/model/workday';
import { HttpErrorResponse } from '@angular/common/http';
import { NgToastService } from 'ng-angular-popup';

@Component({
  selector: 'app-add-workday-modal',
  templateUrl: './add-workday-modal.component.html',
  styleUrls: ['./add-workday-modal.component.scss']
})
export class AddWorkdayModalComponent implements OnInit {

  constructor(public addModalRef: BsModalRef, private dataService: DataService, private workdayService: WorkdayService, private toast: NgToastService) {}
  date!: string 
  workday: Workday = {
    id: 0,
    description: '',
    date: '',
    duration: {
      hours: 0, 
      minutes: 0
    },
    status: ''
  }

  public postWorkday(workday: Workday): void {
    this.workdayService.postWorkday(workday).subscribe({
      next: (response: Workday) => {
        this.toast.success({detail: 'SUCCESS', summary:'Information added', duration: 2000})
      },
      error: (error: HttpErrorResponse) => this.toast.error({detail:'ERROR', summary: error.error.message != undefined ? error.error.message : 'Something went wrong!', sticky:true}),
    });
  }

  isFilled(): boolean {
    let desc = (document.getElementById('desc') as HTMLInputElement).value
    let h = Number((document.getElementById('hours') as HTMLInputElement).value)
    let m = Number((document.getElementById('minutes') as HTMLInputElement).value)
    if (desc == '' || (h == 0 && m == 0)){
      return true;
    }
    return false;
  }
  
  handleAccept(): void {
    this.workday.id = 0
    this.workday.description = (document.getElementById('desc') as HTMLInputElement).value
    this.workday.date = this.dataService.getDate().toLocaleDateString('pl-PL', {day:'2-digit', month: '2-digit', year: 'numeric'})
    this.workday.duration.hours = Number((document.getElementById('hours') as HTMLInputElement).value)
    this.workday.duration.minutes = Number((document.getElementById('minutes') as HTMLInputElement).value)
    this.workday.status = ''
    this.postWorkday(this.workday)
    this.addModalRef.hide()
  }

  ngOnInit(): void {
    this.date = this.dataService.getDate().toLocaleDateString('pl-PL', {day:'2-digit', month: '2-digit', year: 'numeric'})
  }
}