import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { NgToastService } from 'ng-angular-popup';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { WorkerDemand } from 'src/app/model/worker-demand';
import { DataService } from 'src/app/_service/data.service';
import { WorkerdemandService } from 'src/app/_service/workerdemand.service';

import { DemandChangeStatusHrModalComponent } from './demand-change-status-hr-modal/demand-change-status-hr-modal.component';


@Component({
  selector: 'app-worker-demand-hr',
  templateUrl: './worker-demand-hr.component.html',
  styleUrls: ['./worker-demand-hr.component.scss']
})
export class WorkerDemandHrComponent implements OnInit, OnDestroy {

  constructor(private demandService: WorkerdemandService, private toast: NgToastService, private dataService: DataService, private modalService: BsModalService) { }

  demandStatusHrModalRef!: BsModalRef;
  refresh: any;
  displayedColumns: string[] = ['id', 'spec', 'exp', 'skills', 'status', 'action'];
  dataSourceCcmAccepted = new MatTableDataSource<WorkerDemand>([]);
  dataSourceInProgress = new MatTableDataSource<WorkerDemand>([]);

  public getDemands(): void {
    this.demandService.getWorkerDemandByStatus('CCM_ACCEPTED').subscribe({
      next: (response1: WorkerDemand[]) => {
        this.dataSourceCcmAccepted = new MatTableDataSource<WorkerDemand>(response1);
        this.demandService.getWorkerDemandByStatus('IN_PROGRESS').subscribe({
          next: (response2: WorkerDemand[]) => {
            this.dataSourceInProgress = new MatTableDataSource<WorkerDemand>(response2);
          },
          error: (error: HttpErrorResponse) => this.toast.error({detail:'ERROR', summary: error.error.message != undefined ? error.error.message : 'Something went wrong!', sticky:true}),
        })
      },
      error: (error: HttpErrorResponse) => this.toast.error({detail:'ERROR', summary: error.error.message != undefined ? error.error.message : 'Something went wrong!', sticky:true}),
    })
  }

  openChangeStatusModal(id: number): void {
    this.dataService.setDemandId(id)
    this.demandStatusHrModalRef = this.modalService.show(DemandChangeStatusHrModalComponent);
    this.demandStatusHrModalRef.onHide?.subscribe(()=> {
      this.getDemands()
    })
  }

  ngOnInit(): void {
    this.getDemands()
    this.refresh = setInterval(() => this.getDemands(), 3000)
  }
  ngOnDestroy(): void {
    clearInterval(this.refresh)
  }

}
