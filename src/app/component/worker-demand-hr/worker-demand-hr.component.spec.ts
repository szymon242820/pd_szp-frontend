import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkerDemandHrComponent } from './worker-demand-hr.component';

describe('WorkerDemandHrComponent', () => {
  let component: WorkerDemandHrComponent;
  let fixture: ComponentFixture<WorkerDemandHrComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WorkerDemandHrComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(WorkerDemandHrComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
