import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DemandChangeStatusHrModalComponent } from './demand-change-status-hr-modal.component';

describe('DemandChangeStatusHrModalComponent', () => {
  let component: DemandChangeStatusHrModalComponent;
  let fixture: ComponentFixture<DemandChangeStatusHrModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DemandChangeStatusHrModalComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DemandChangeStatusHrModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
