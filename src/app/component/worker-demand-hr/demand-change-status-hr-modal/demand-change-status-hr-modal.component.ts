import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgToastService } from 'ng-angular-popup';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { WorkerDemand } from 'src/app/model/worker-demand';
import { DataService } from 'src/app/_service/data.service';
import { WorkerdemandService } from 'src/app/_service/workerdemand.service';

@Component({
  selector: 'app-demand-change-status-hr-modal',
  templateUrl: './demand-change-status-hr-modal.component.html',
  styleUrls: ['./demand-change-status-hr-modal.component.scss']
})
export class DemandChangeStatusHrModalComponent implements OnInit {

  constructor(public demandStatusHrModalRef: BsModalRef, private dataService: DataService, private demandService: WorkerdemandService, private toast: NgToastService) { }

  demand: WorkerDemand = {id: 0, exp: '', spec: {id: 0, name: '', description: ''}, skills: [{ id: 0, name: '', description: ''}], status: ''}
  id!: number
  status!: string

  handleAccept(): void {
    this.status = (document.getElementById("status") as HTMLInputElement).value
    this.patchDemand(this.id, this.status)
    this.demandStatusHrModalRef.hide()
  }

  public getDemandById(id:number): void {
    this.demandService.getWorkerDemandById(id).subscribe({
      next: (response: WorkerDemand) => this.demand = response,
      error: (error: HttpErrorResponse) => this.toast.error({detail:'ERROR', summary: error.message, sticky:true}),
    })
  }

  public patchDemand(id: number, status: string): void {
    this.demandService.patchWorkerDemand(id, status).subscribe({
      next: (response: WorkerDemand) => this.toast.success({detail: 'SUCCESS', summary: 'Status changed', duration: 2000}),
      error: (error: HttpErrorResponse) => this.toast.error({detail:'ERROR', summary: error.message, sticky:true}),
    });
  }


  ngOnInit(): void {
    this.id = this.dataService.getDemandId()
    this.getDemandById(this.id)
  }

}
