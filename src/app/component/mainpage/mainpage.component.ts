import { HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgToastService } from 'ng-angular-popup';
import { Joboffer } from 'src/app/model/joboffer';
import { PageResponse } from 'src/app/model/page-response';
import { JobofferService } from 'src/app/_service/joboffer.service';

@Component({
  selector: 'app-mainpage',
  templateUrl: './mainpage.component.html',
  styleUrls: ['./mainpage.component.scss']
})

export class MainpageComponent implements OnInit {

  constructor(private jobofferService: JobofferService, private toast: NgToastService) { }

  joboffers: Joboffer[] = []
  isLoggedIn: boolean = JSON.parse(sessionStorage.getItem('isLoggedIn')!)

  public getJoboffers(): void {
    this.jobofferService.getJoboffers(0, 3).subscribe({
      next: (response: PageResponse) => { this.joboffers = <Joboffer[]>response.result},
      error: (error: HttpErrorResponse) => this.toast.error({detail:'ERROR', summary: error.error.message != undefined ? error.error.message : 'Something went wrong!', sticky:true}),
    })
  }
  
  ngOnInit(): void {
    this.getJoboffers()
    if (this.isLoggedIn == null){
      this.isLoggedIn = false
    }
  }
}
