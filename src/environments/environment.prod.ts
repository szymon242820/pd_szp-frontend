export const environment = {
  production: true,
  apiVersion: 'v1',
  backendUrl: 'http://localhost:8080/api',
  frontendUrl: 'http://localhost:4200/',
};